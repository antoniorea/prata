// ex61.cc --  upper to lower & viceversa
// cctypes.cpp -- using the ctype.h library
#include <iostream>
#include <cctype>                   // prototypes for character functions
int main()
{
    std::cout << "Enter text for analysis, and type @"
            " to terminate input.\n";
    char ch;
    std::cin.get(ch);                // get first character
    while (ch != '@')               // test for sentinel
    {
        if(!(isdigit(ch)))
        {
            if (isupper(ch))        // is it an alphabetic character?
                std::cout.put(tolower(ch));
            else if (islower(ch))   // is it a whitespace character?
                std::cout.put(toupper(ch));
            else 
                std::cout.put(ch);
        }
        std::cin.get(ch);            // get next character
    }   
    return 0;
}
