// ex62.cc -- donations array
#include <iostream>
#include <cctype>            // prototypes for character functions

int main()
{
    const int SIZE{10};
    size_t count{}, i{};
    double donations[SIZE], total{};
    
    std::cout << "Please enter #1 donation : ";
    while((i < SIZE)  && std::cin >> donations[i])
    {
        total += donations[i];
        ++i;
        if( i < SIZE )
        std::cout << "Please enter #"<< i + 1 <<" donation :";
    }

    double average = total / i;

    for(auto elemento : donations )
            if (elemento > average)
                ++count;

    std::cout << "\nAverage : " << average <<std::endl;
    std::cout << count << " : numbers in the array are larger than the average.: "
    << average <<std::endl;
    return 0;
}